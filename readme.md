Task from QualiumSystems 
================


# Table of Contents
- [Deploy with Docker](#deploy-with-docker)

#Deploy with Docker

1 . Go to folder with project (e.g. exams).
2 . In 'hosts' file on your computer (in ubuntu path to file: /etc/hosts) you should add next line.
```
127.0.0.1 exams.dev
```
 
3 . If you need change some settings, you can do it in:
    exams/client/src/config.js - for frontend,
    exams/server/config - for backend.
    
Also if you need change some settings for deploy client, server, mongo, you can do it in:
    docker-compose.yml - for development, 

4 . Go to folder with project (e.g. exams) and use shell run the command: 
```
docker-compose up -d --build
```
5 . In you browser open http://exams.dev/

5 . For stop docker use the command Ctrl+C or: 
```
docker-compose stop
```