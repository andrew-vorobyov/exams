import React from 'react';
import {Route, Link} from 'react-router-dom';

import GroupsPage from './components/pages/GroupsPage/GroupsPage';
import FormAddGroup from './components/pages/GroupsPage/componentsPage/FormAddGroup';
import FormEditGroup from './components/pages/GroupsPage/componentsPage/FormEditGroup';

import LoginPage from './components/pages/LoginPage/LoginPage';
import LogoutPage from './components/pages/LogoutPage/LogoutPage';
import RegistrationPage from './components/pages/RegistrationPage/RegistrationPage';

// ******************
// ***** ROUTES *****
// ******************
export const openRoutes = [
    <Route key={'LoginPage'} path="/login" component={LoginPage}/>,
    <Route key={'RegistrationPage'} exact path="/registration" component={RegistrationPage}/>,
];
export const adminRoutes = [
    <Route key={'GroupsPage'} exact path="/groups" component={GroupsPage}/>,
    <Route key={'FormAddGroup'} exact path="/groups/add" component={FormAddGroup}/>,
    <Route key={'FormEditGroup'} exact path="/groups/edit/:id" component={FormEditGroup}/>,
    <Route key={'LogoutPage'} exact path="/logout" component={LogoutPage}/>,
];
export const subscriberRoutes = [
    <Route key={'GroupsPage'} exact path="/groups" component={GroupsPage}/>,
    <Route key={'FormAddGroup'} exact path="/groups/add" component={FormAddGroup}/>,
    <Route key={'FormEditGroup'} exact path="/groups/edit/:id" component={FormEditGroup}/>,
    <Route key={'LogoutPage'} exact path="/logout" component={LogoutPage}/>,
];

// *****************
// ***** LINKS *****
// *****************
export const openLinks = [
    <li key={'Registration'}><Link to="/registration">Registration</Link></li>,
    <li key={'Login'}><Link to="/login">Login</Link></li>
];
export const adminLinks = [
    <li key={'Groups'}><Link to="/groups">Groups</Link></li>,
    <li key={'Logout'}><Link to="/logout">Logout</Link></li>
];
export const subscriberLinks = [
    <li key={'Groups'}><Link to="/groups">Groups</Link></li>,
    <li key={'Logout'}><Link to="/logout">Logout</Link></li>
];