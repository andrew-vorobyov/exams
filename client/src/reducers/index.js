import {combineReducers} from 'redux';

import reducerAuth from './reducerAuth'
import reducerCheckAdmin from './reducerCheckAdmin'

const rootReducer = combineReducers({
    authStatus: reducerAuth,
    resultCheckAdmin:reducerCheckAdmin,
})

export default rootReducer;