import React, {Component} from 'react';
import Groups from './componentsPage/Groups'

class GroupsPage extends Component {
    render() {
        return (
            <div className="GroupPage">
                <Groups history={this.props.history}></Groups>
            </div>
        );
    }
}

export default GroupsPage;