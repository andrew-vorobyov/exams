import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {authUser} from '../../../actions/index';
import {authUrl} from '../../../config';
import './LoginPage.css';

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',

            errors: []
        };
        this.handleFormUsername = this.handleFormUsername.bind(this);
        this.handleFormPassword = this.handleFormPassword.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.renderErrors = this.renderErrors.bind(this);
    }

    handleFormUsername(event) {
        this.setState({username: event.target.value});
    }

    handleFormPassword(event) {
        this.setState({password: event.target.value});
    }

    handleFormSubmit(event) {
        event.preventDefault();

        this.setState({errors: []});

        const self = this;

        axios.post(authUrl, {
            username: this.state.username,
            password: this.state.password
        }).then(res => {

            const token = res.data.token;
            const email = res.data.email;
            localStorage.setItem('token', token);
            localStorage.setItem('email', email);
            this.props.authUser();
            this.props.history.push('/groups');
        }).catch(function (error) {
            const newErrors = [...self.state.errors]
            newErrors.push('User not found')
            self.setState({errors: newErrors});
        });
    }

    renderErrors() {
        if (this.state.errors.length !== 0) {
            return this.state.errors.map((error, i) => <li key={i}>{error}</li>);
        }
    }

    componentWillMount(){
        if (localStorage.getItem('token')) {
            this.props.history.push('/tests');
        }
    }

    render() {

        return (
            <div className="LoginPage">

                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <div className="panel panel-login">
                                <div className="page-header">
                                    <h2>Login</h2>
                                </div>
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form id="login-form" onSubmit={this.handleFormSubmit}
                                                  style={{display: 'block'}}>
                                                <div className="form-group">
                                                    <label htmlFor="username">Username</label>
                                                    <input required type="text" name="username" id="username"
                                                           tabIndex="1"
                                                           className="form-control" placeholder="Username"
                                                           value={this.state.username}
                                                           onChange={this.handleFormUsername}/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="password">Password</label>
                                                    <input required type="password" name="password" id="password"
                                                           tabIndex="2"
                                                           className="form-control" placeholder="Password"
                                                           value={this.state.password}
                                                           onChange={this.handleFormPassword}/>
                                                </div>

                                                <div className="errors">
                                                    <ul>
                                                        {this.renderErrors()}
                                                    </ul>
                                                </div>


                                                <div className="form-group">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-sm-offset-3">
                                                            <input type="submit" name="login-submit" id="login-submit"
                                                                   tabIndex="4" className="form-control btn-block btn btn-success"
                                                                   style={{height: '40px'}}
                                                                   value="Log In"/>
                                                            <br/>

                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default LoginPage = connect(
    (state) => {
        return {authStatus: state.authStatus}
    },
    (dispatch) => {
        return bindActionCreators({authUser: authUser}, dispatch)
    }
)(LoginPage);