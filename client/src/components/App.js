import React, {Component} from 'react';
import logo from './../images/logo.png';
import {BrowserRouter as Router} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import {connect} from 'react-redux';
import './App.css';
import {checkAdmin} from '../actions/index';
import {bindActionCreators} from 'redux';

import {openRoutes, adminRoutes, subscriberRoutes} from '../routes';
import {openLinks, adminLinks, subscriberLinks} from '../routes';

import {Link} from 'react-router-dom';

class App extends Component {

    constructor(props) {
        super(props);
        this.renderLinks = this.renderLinks.bind(this);
        this.renderRoutes = this.renderRoutes.bind(this);
    }

    renderLinks() {

        if (this.props.authStatus || localStorage.getItem('token') !== null) {

            this.props.checkAdmin();

            if (!this.props.resultCheckAdmin) {
                return subscriberLinks;
            } else {
                return adminLinks;
            }
        } else {
            return openLinks;
        }
    }

    renderRoutes() {
        if (this.props.authStatus || localStorage.getItem('token')) {

            this.props.checkAdmin();

            if (!this.props.resultCheckAdmin) {
                return subscriberRoutes;
            } else {
                return adminRoutes;
            }
        }

        return openRoutes;
    }

    render() {

        return (
            <div className="App">
                <Router history={createHistory}>
                    <div id="wrapper" className="Router">
                        <header id="header">
                            <nav className="navbar navbar-default">
                                <div className="container-fluid">
                                    <div className="navbar-header">
                                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span className="sr-only">Toggle navigation</span>
                                            <span className="icon-bar"></span>
                                            <span className="icon-bar"></span>
                                            <span className="icon-bar"></span>
                                        </button>

                                        <Link to="/" className="navbar-brand"><img src={logo} className="App-logo"
                                                                                   alt="logo"/></Link>

                                    </div>
                                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                        <ul className="nav navbar-nav navbar-right">
                                            {this.renderLinks()}
                                        </ul>

                                    </div>
                                </div>
                            </nav>
                        </header>
                        <main role="main" id="main">
                            <div className="container main-container">

                                {this.renderRoutes()}

                            </div>
                        </main>
                    </div>
                </Router>

            </div>
        );
    }
}

export default App = connect(
    (state) => {
        return {authStatus: state.authStatus, resultCheckAdmin: state.resultCheckAdmin}
    },
    (dispatch) => {
        return bindActionCreators({checkAdmin: checkAdmin}, dispatch)
    }
)(App);