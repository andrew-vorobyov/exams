/**
 * Module dependencies.
 */

var express = require('express');
var router = express.Router();
var helpers = require('../helpers');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongoose').Types.ObjectId;
var md5 = require('js-md5');

/**
 * Module configurations.
 */

var config = require('../config');

/**
 * Routes.
 */

router.get('/all', function (req, res, next) {

    // get data from db, response
    MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
        if (err) throw err;

        try {
            db.collection('groups').find({}, {hash: 0}).toArray(function (err, result) {
                if (err) throw err;

                if (result.length == 0) {
                    res.status(404);
                    res.send(JSON.stringify({'error': 'groups not found'}));
                } else {
                    res.send({'result': result});
                }
            })

        } catch (e) {
            console.log(e);
            res.status(400);
            res.send(JSON.stringify({'error': 'error'}));
        }

    })
});

router.get('/allgroups', function (req, res, next) {

    // get data from db, response
    MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
        if (err) throw err;

        try {
            db.collection('groups').find({}).count(function (err, count) {
                if (err) throw err;

                db.collection('groups').find({}, {hash: 0}).skip((parseInt(req.query.pageNumber) - 1) * 10).limit(10).toArray(function (err, result) {
                    if (err) throw err;

                    if (result.length == 0) {
                        res.status(404);
                        res.send(JSON.stringify({'error': 'groups not found'}));
                    } else {
                        var response = {result: result, count: count};
                        res.send({'result': response});
                    }
                })
            });

        } catch (e) {
            console.log(e);
            res.status(400);
            res.send(JSON.stringify({'error': 'error'}));
        }

    })
});


router.delete('/:id', function (req, res, next) {

    if (!req.headers.authorization) {
        res.status(401);
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var resultCheckToken = helpers.checkToken(req.headers.authorization);
        var userEmail = helpers.getDataFromToken(req.headers.authorization).email;
        var resultCheckAdmin = helpers.checkAdmin(userEmail);

        if (!resultCheckToken && !resultCheckAdmin) {
            res.status(401);
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire or you do not have need permission'}))
        } else {

            MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
                if (err) throw err;

                try {
                    var id = req.params.id;

                    // 1. get groupName
                    db.collection('groups').find({"_id": ObjectId(id)}, {hash: 0}).toArray(function (err, group) {
                        if (err) throw err;

                        if (group.length > 0) {
                            var groupName = group[0].group;

                            // 2. get questions (id and group fields)
                            db.collection('questions').find({group: {$all: [groupName]}}, {
                                correctAnswer: 0,
                                hash: 0,
                                title: 0,
                                answers: 0
                            }).toArray(function (err, questionsWithThisGroup) {
                                if (err) throw err;
                                if (questionsWithThisGroup.length > 0) {

                                    // 3. create new group
                                    questionsWithThisGroup.forEach(function (question) {
                                        var idFinal = question._id;
                                        var groupNow = question.group;
                                        var groupFinal = [];

                                        if (groupNow.length <= 1 || typeof(groupNow) == 'string') {
                                        } else {
                                            groupNow.forEach(function (gr) {
                                                if (gr != groupName) {
                                                    groupFinal.push(gr)
                                                }
                                            })
                                        }

                                        // 4. update group in question
                                        db.collection('questions').updateOne({"_id": ObjectId(idFinal)}, {$set: {group: groupFinal}}, function (err, result) {
                                            if (err) throw err
                                        });
                                    })
                                }
                            })
                        }
                    });

                    // delete group
                    db.collection('groups').removeOne({"_id": ObjectId(id)});
                    res.send({'result': 'group have been deleted'});
                } catch (e) {
                    res.status(400);
                    res.send(JSON.stringify({'error': 'error'}));
                }
            })
        }
    }
});

router.post('/add', function (req, res) {

    if (!req.headers.authorization) {
        res.status(401);
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var resultCheckToken = helpers.checkToken(req.headers.authorization);
        var userEmail = helpers.getDataFromToken(req.headers.authorization).email;
        var resultCheckAdmin = helpers.checkAdmin(userEmail);

        if (!resultCheckToken && !resultCheckAdmin) {
            res.status(401);
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire or you do not have need permission'}))
        } else {

            MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
                if (err) throw err;

                var groupAfterTrim = req.body.group.trim();
                var hashGroup = md5(groupAfterTrim);

                db.collection('groups').find({"hash": hashGroup}).toArray(function (err, result) {
                    if (err) throw err;

                    if (result.length == 0) {
                        try {
                            var groupFinal = {
                                group: groupAfterTrim,
                                hash: hashGroup
                            };

                            db.collection('groups').insertOne(groupFinal);
                            res.send({'result': 'group has added'});
                        } catch (e) {
                            console.log(e);
                            res.status(400);
                            res.send(JSON.stringify({'error': 'group have not been created'}));
                        }
                    } else {
                        res.status(400);
                        res.send(JSON.stringify({'error': 'group have not been created, group with this name have already existed'}));
                    }
                })
            })
        }
    }
});

router.get('/:id', function (req, res, next) {

    if (!req.headers.authorization) {
        res.status(401);
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var resultCheckToken = helpers.checkToken(req.headers.authorization);
        var userEmail = helpers.getDataFromToken(req.headers.authorization).email;
        var resultCheckAdmin = helpers.checkAdmin(userEmail);

        if (!resultCheckToken && !resultCheckAdmin) {
            res.status(401);
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire or you do not have need permission'}))
        } else {

            var id = req.params.id;
            MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
                if (err) throw err;

                try {
                    db.collection('groups').find({"_id": ObjectId(id)}, {hash: 0}).toArray(function (err, result) {
                        if (err) throw err;
                        if (result.length == 0) {
                            res.status(404);
                            res.send(JSON.stringify({'error': 'group not found'}));
                        } else {
                            res.send({'result': result});
                        }
                    })
                } catch (e) {
                    console.log(e);
                    res.status(400);
                    res.send(JSON.stringify({'error': 'error'}));
                }
            })
        }
    }
});

router.post('/edit', function (req, res) {

    if (!req.headers.authorization) {
        res.status(401);
        res.send(JSON.stringify({'error': 'you request do not have token'}));
    } else {

        var resultCheckToken = helpers.checkToken(req.headers.authorization);
        var userEmail = helpers.getDataFromToken(req.headers.authorization).email;
        var resultCheckAdmin = helpers.checkAdmin(userEmail);

        if (!resultCheckToken && !resultCheckAdmin) {
            res.status(401);
            res.send(JSON.stringify({'error': 'your token is not valid or have been expire or you do not have need permission'}))
        } else {

            MongoClient.connect(config.connectDatabaseUrl, function (err, db) {
                if (err) throw err;

                try {
                    var id = req.body._id;
                    var newGroupAfterTrim = req.body.group.trim();
                    var hashGroup = md5(newGroupAfterTrim);
                    var newGroupFinal = {
                        group: newGroupAfterTrim,
                        hash: hashGroup
                    };

                    // 1. get groupName
                    db.collection('groups').find({"_id": ObjectId(id)}, {hash: 0}).toArray(function (err, group) {
                        if (err) throw err;

                        if (group.length > 0) {
                            var groupName = group[0].group;

                            // 2. get questions (id and group fields)
                            db.collection('questions').find({group: {$all: [groupName]}}, {
                                correctAnswer: 0,
                                hash: 0,
                                title: 0,
                                answers: 0
                            }).toArray(function (err, questionsWithThisGroup) {
                                if (err) throw err;

                                if (questionsWithThisGroup.length > 0) {

                                    // 3. create new group
                                    questionsWithThisGroup.forEach(function (question) {
                                        var idFinal = question._id;
                                        var groupNow = question.group;
                                        var groupFinal = [];

                                        if (groupNow.length <= 1 || typeof(groupNow) == 'string') {
                                            groupFinal.push(newGroupAfterTrim);
                                        } else {
                                            groupNow.forEach(function (gr) {
                                                if (gr == groupName) {
                                                    groupFinal.push(newGroupAfterTrim);
                                                } else {
                                                    groupFinal.push(gr);
                                                }
                                            })
                                        }

                                        // 4. update group in question
                                        db.collection('questions').updateOne({"_id": ObjectId(idFinal)}, {$set: {group: groupFinal}}, function (err, result) {
                                            if (err) throw err
                                        });
                                    })
                                }
                            })
                        }
                    });

                    var result = db.collection('groups').updateOne({"_id": ObjectId(id)}, {$set: newGroupFinal}, function (err, result) {
                        if (err) throw err
                    });
                    res.send({'result': result});

                } catch (e) {
                    console.log(e);
                    res.status(400);
                    res.send(JSON.stringify({'error': 'question have not been created'}));
                }
            })
        }
    }
});


module.exports = router;
