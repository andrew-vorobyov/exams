/**
 * Module dependencies.
 */

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
var mongoose = require('mongoose');

/**
 * Module Routes
 */

var index = require('./routes/index');
var users = require('./routes/users');
var groups = require('./routes/groups');
var auth = require('./routes/auth');
var registration = require('./routes/registration');

/**
 * Module configurations
 */

var config = require('./config');

/**
 * Create Express server.
 */

var app = express();

/**
* Connect to MongoDB.
*/

mongoose.connect(config.connectDatabaseUrl);

/**
 * Express configuration.
 */

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());


// Middleware for all routes (CORS and so on).
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, id, authorization');

    next();
};
app.use(allowCrossDomain);

// routes
app.use('/', index);
app.use('/users', users);
app.use('/groups', groups);
app.use('/auth', auth);
app.use('/registration', registration);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
